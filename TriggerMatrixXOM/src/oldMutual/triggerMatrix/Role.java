package oldMutual.triggerMatrix;

public class Role {
	
	private String owner;
	private String insured;
	private String beneficiary;
	private String replacementOwner;
	private String premiumPayer;
	
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getInsured() {
		return insured;
	}
	public void setInsured(String insured) {
		this.insured = insured;
	}
	public String getBeneficiary() {
		return beneficiary;
	}
	public void setBeneficiary(String beneficiary) {
		this.beneficiary = beneficiary;
	}
	public String getReplacementOwner() {
		return replacementOwner;
	}
	public void setReplacementOwner(String replacementOwner) {
		this.replacementOwner = replacementOwner;
	}
	public String getPremiumPayer() {
		return premiumPayer;
	}
	public void setPremiumPayer(String premiumPayer) {
		this.premiumPayer = premiumPayer;
	}

}
