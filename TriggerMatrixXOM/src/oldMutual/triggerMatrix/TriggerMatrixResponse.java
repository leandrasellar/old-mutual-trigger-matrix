package oldMutual.triggerMatrix;

import java.util.ArrayList;

public class TriggerMatrixResponse {
	
	private ArrayList<String> triggeredProcessList;
	
	public TriggerMatrixResponse(){
		this.triggeredProcessList = new ArrayList<>();
	}
	

	public ArrayList<String> getTriggeredProcesses() {
		return triggeredProcessList;
	}

	public void setTriggeredProcesses(ArrayList<String> triggeredProcesses) {
		this.triggeredProcessList = triggeredProcesses;
	}
	
	
	public void addTriggeredProcess(String process){
		//System.out.println("Add String " + process);
		this.triggeredProcessList.add(process);
	}


	@Override
	public String toString() {
		return "TriggerMatrixResponse [triggeredProcessList="
				+ triggeredProcessList + "]";
	}

}
