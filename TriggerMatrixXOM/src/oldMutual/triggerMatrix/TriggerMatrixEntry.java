package oldMutual.triggerMatrix;

public class TriggerMatrixEntry {
	
	private String trigger;
	private String benefitCategory;
	private String benefitType;
	private Role primaryRole;
	private Role secondaryRole;
	
	public String getTrigger() {
		return trigger;
	}
	public void setTrigger(String trigger) {
		this.trigger = trigger;
	}
	public String getBenefitCategory() {
		return benefitCategory;
	}
	public void setBenefitCategory(String benefitCategory) {
		this.benefitCategory = benefitCategory;
	}
	public String getBenefitType() {
		return benefitType;
	}
	public void setBenefitType(String benefitType) {
		this.benefitType = benefitType;
	}
	public Role getPrimaryRole() {
		return primaryRole;
	}
	public void setPrimaryRole(Role primaryRole) {
		this.primaryRole = primaryRole;
	}
	public Role getSecondaryRole() {
		return secondaryRole;
	}
	public void setSecondaryRole(Role secondaryRole) {
		this.secondaryRole = secondaryRole;
	}

}
