<!-- Licensed Materials - Property of IBM                           -->
<!-- 5725-B69 5655-Y17                                              -->
<!-- Copyright IBM Corp. 2013, 2017. All Rights Reserved            -->
<!-- US Government Users Restricted Rights - Use, duplication or    -->
<!-- disclosure restricted by GSA ADP Schedule Contract with        -->
<!-- IBM Corp.                                                      -->
<project default="run.xl" name="query execution sample" basedir=".">
	
	<property environment="myenv" />
	<property name="xl.url" value="xl://./data/script/TriggerMatrix.xlsx" />
	<property name="xl.sheet" value="sheet" />
    <property name="ds.home" location="${basedir}/../../.." />
    <property name="derby.lib" location="${ds.home}/shared/tools/derby/lib"/>
	
	<condition property="eclipse.home" value="${myenv.ECLIPSE_HOME}">
		<isset property="myenv.ECLIPSE_HOME" />
	</condition>
	
	
	<fail unless="eclipse.home" message="Please set up the ECLIPSE_HOME environment variable." />
	<property name="eclipsedir" value="${eclipse.home}" />
	<echo message="Eclipse dir=${eclipsedir}"/>

	<property name="path" value="${eclipsedir}/studio" />
		
	<target name="run.xl" depends="init.runtime,load.dt.xl" />
	
	<target name="init">
		<path id="eclipse.launcher">
			<fileset dir="${eclipsedir}/plugins">
				<include name="org.eclipse.equinox.launcher_*.jar"/>
			</fileset>
		</path>
		<available property="found_launcher.jar" resource="launcher.properties">
			<classpath refid="eclipse.launcher" />
		</available>

		<echo message="Using ECLIPSE_HOME=${eclipsedir}" />
		<fail unless="found_launcher.jar" message="Cannot find org.eclipse.equinox.launcher archive in the ${eclipsedir}/plugins folder." />
	</target>
	
	<target name="init.runtime" depends="init">
		<!-- modify this target if os, ws or arch properties do not match your runtime environment -->
		<condition property="os" value="linux" >
			<equals arg1="${os.name}" arg2="Linux" />
		</condition>
		<condition property="os" value="solaris" >
  			<equals arg1="${os.name}" arg2="SunOS" />
		</condition>
		<property name="os" value="win32"/>
		<condition property="ws" value="gtk" >
			<equals arg1="${os.name}" arg2="Linux" />
		</condition>
		<condition property="ws" value="gtk" >
			<equals arg1="${os.name}" arg2="SunOS" />
		</condition>
		<property name="ws" value="win32"/>
		<condition property="arch" value="sparc" >
			<equals arg1="${os.name}" arg2="SunOS" />
		</condition>
		<property name="arch" value="x86"/>	
	</target>

	<macrodef name="headless.application">
       <attribute name="workspace"/>
	   <attribute name="targetproject"/>
	   <attribute name="dtname"/>		
	   <attribute name="driver"/>
	   <attribute name="url"/>
	   <attribute name="user"/>
	   <attribute name="pwd"/>	
	   <attribute name="info"/>
	   <sequential>
		<echo>Loading decision table project=@{targetproject} decisiontable=@{dtname} driver=@{driver} url=@{url} user=@{user} pwd=@{pwd} info=@{info} </echo>
		<java classname="org.eclipse.core.launcher.Main" fork="true" failonerror="false" resultproperty="javaresultcode">
			<jvmarg value="-Xmx512M" />
			<!--jvmarg value="-Dderby.stream.error.file=${db.log}" /-->
			<classpath refid="eclipse.launcher" />

			<arg line="-application C:\Users\Leandra\IBM\ODM\DS\triggermatrix" />
			<!-- <arg line="-os ${os}"/>
			<arg line="-ws ${ws}"/>
			<arg line="-arch ${arch}"/> -->
			<arg line="-nl en_US" />
			<arg line="-consolelog" />
			<arg line="-data '@{workspace}'" />
			<arg line="-targetproject '@{targetproject}'" />
			<arg line="-dtname '@{dtname}'" />
			<arg line="-driver '@{driver}'" />
			<arg line="-login '@{user}'" />
			<arg line="-pwd '@{pwd}'" />			
			<arg line="-info '@{info}'" />
			<arg line="-url '@{url}'" />
			<arg line="-debug" />
		</java>
		
		<condition property="ko_result_code">
			<equals arg1="${javaresultcode}" arg2="13" />
		</condition>
		<fail if="ko_result_code" message="Error during populate operation. Probable cause is a bad installation of the sample and/or its plugin." />
		<condition property="ok_result_code">
				<equals arg1="${javaresultcode}" arg2="0" />
		</condition>
		<fail unless="ok_result_code" message="Error during query execution." />
	   </sequential>
	</macrodef>		
	
	<target name="load.dt.xl" >
			<headless.application 
				workspace="${triggermatrix}"
				targetproject="TriggerMatrixRules" 
				dtname="TM" 
				driver=" " 
				url="${xl.url}" 
				user=" " 
				pwd=" " 
				info="${xl.sheet}" 
			/>
	</target>
</project>
