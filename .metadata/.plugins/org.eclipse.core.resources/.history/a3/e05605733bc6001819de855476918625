<!-- Licensed Materials - Property of IBM                           -->
<!-- 5725-B69 5655-Y17                                              -->
<!-- Copyright IBM Corp. 2013, 2017. All Rights Reserved            -->
<!-- US Government Users Restricted Rights - Use, duplication or    -->
<!-- disclosure restricted by GSA ADP Schedule Contract with        -->
<!-- IBM Corp.                                                      -->
<project default="run.file" name="query execution sample" basedir=".">
	
	<property environment="myenv" />
	<property name="xl.url" value="xl://./data/script/TriggerMatrix.xlsx" />
	<property name="xl.sheet" value="sheet" />
    <property name="ds.home" location="${basedir}/../../.." />
    <property name="derby.lib" location="${ds.home}/shared/tools/derby/lib"/>
	
	<condition property="eclipse.home" value="${myenv.ECLIPSE_HOME}">
		<isset property="myenv.ECLIPSE_HOME" />
	</condition>
	
	
	<fail unless="eclipse.home" message="Please set up the ECLIPSE_HOME environment variable." />
	<property name="eclipsedir" value="${eclipse.home}" />
	<echo message="Eclipse dir=${eclipsedir}"/>

	<property name="path" value="${eclipsedir}/studio" />
		
	<target name="run.xl" depends="init.runtime,load.dt.xl" />
	
	<target name="init">
		<path id="eclipse.launcher">
			<fileset dir="${eclipsedir}/plugins">
				<include name="org.eclipse.equinox.launcher_*.jar"/>
			</fileset>
		</path>
		<available property="found_launcher.jar" resource="launcher.properties">
			<classpath refid="eclipse.launcher" />
		</available>

		<echo message="Using ECLIPSE_HOME=${eclipsedir}" />
		<fail unless="found_launcher.jar" message="Cannot find org.eclipse.equinox.launcher archive in the ${eclipsedir}/plugins folder." />
	</target>
	
	<target name="init.runtime" depends="init">
		<!-- modify this target if os, ws or arch properties do not match your runtime environment -->
		<condition property="os" value="linux" >
			<equals arg1="${os.name}" arg2="Linux" />
		</condition>
		<condition property="os" value="solaris" >
  			<equals arg1="${os.name}" arg2="SunOS" />
		</condition>
		<property name="os" value="win32"/>
		<condition property="ws" value="gtk" >
			<equals arg1="${os.name}" arg2="Linux" />
		</condition>
		<condition property="ws" value="gtk" >
			<equals arg1="${os.name}" arg2="SunOS" />
		</condition>
		<property name="ws" value="win32"/>
		<condition property="arch" value="sparc" >
			<equals arg1="${os.name}" arg2="SunOS" />
		</condition>
		<property name="arch" value="x86"/>	
	</target>

	<macrodef name="headless.application">
       <attribute name="workspace"/>
	   <attribute name="targetproject"/>
	   <attribute name="dtname"/>		
	   <attribute name="driver"/>
	   <attribute name="url"/>
	   <attribute name="user"/>
	   <attribute name="pwd"/>	
	   <attribute name="info"/>
	   <sequential>
		<echo>Loading decision table project=@{targetproject} decisiontable=@{dtname} driver=@{driver} url=@{url} user=@{user} pwd=@{pwd} info=@{info} </echo>
		<java classname="org.eclipse.core.launcher.Main" fork="true" failonerror="false" resultproperty="javaresultcode">
			<jvmarg value="-Xmx512M" />
			<jvmarg value="-Dderby.stream.error.file=${db.log}" />
			<classpath refid="eclipse.launcher" />

			<arg line="-application ilog.rules.studio.samples.dtpopulate.headless.application" />
			<!-- <arg line="-os ${os}"/>
			<arg line="-ws ${ws}"/>
			<arg line="-arch ${arch}"/> -->
			<arg line="-nl en_US" />
			<arg line="-consolelog" />
			<arg line="-data '@{workspace}'" />
			<arg line="-targetproject '@{targetproject}'" />
			<arg line="-dtname '@{dtname}'" />
			<arg line="-driver '@{driver}'" />
			<arg line="-login '@{user}'" />
			<arg line="-pwd '@{pwd}'" />			
			<arg line="-info '@{info}'" />
			<arg line="-url '@{url}'" />
			<arg line="-debug" />
		</java>
		
		<condition property="ko_result_code">
			<equals arg1="${javaresultcode}" arg2="13" />
		</condition>
		<fail if="ko_result_code" message="Error during populate operation. Probable cause is a bad installation of the sample and/or its plugin." />
		<condition property="ok_result_code">
				<equals arg1="${javaresultcode}" arg2="0" />
		</condition>
		<fail unless="ok_result_code" message="Error during query execution." />
	   </sequential>
	</macrodef>

	<target name="create.schema">
		<java classname="org.apache.derby.tools.ij" fork="true" failonerror="true" resultproperty="javaresultcode">
			<jvmarg value="-Xmx256M" />
			<jvmarg value="-Dderby.stream.error.file=${db.log}" />
			<jvmarg value="-Dij.driver=${db.driver}" />
			<jvmarg value="-Dij.database=${db.url};${db.option};user=${db.user};password=${db.pwd}" /> 
	         <classpath>
	           <pathelement location="${derby.lib}/derby.jar"/>
	           <pathelement location="${derby.lib}/derbyclient.jar"/>
	           <pathelement location="${derby.lib}/derbytools.jar"/>
	         </classpath>
			 <arg line="${db.script}" />
		</java>
		<condition property="ok_result_code">
				<equals arg1="${javaresultcode}" arg2="0" />
		</condition>
		<fail unless="ok_result_code" message="Error during database schema creation." />
		<echo>Schema created...</echo>
	</target>
	
	<target name="check.database.started">
		<condition property="database.started">
			<socket server="${db.host}" port="${db.port}"/>
		</condition>		
	</target>
	
	<target name="start.database" depends="check.database.started" unless="database.started">
		<java classname="org.apache.derby.drda.NetworkServerControl" fork="true" spawn="true" > <!--  dir=".." -->
		 <jvmarg value="-Dderby.stream.error.file=${db.log}" />
		 <jvmarg value="-Dderby.drda.logConnections=true"/>
	         <classpath>
	           <pathelement location="${derby.lib}/derby.jar"/>
	           <pathelement location="${derby.lib}/derbynet.jar"/>
	         </classpath>
			 <arg line="start" />
			 <arg line="-h ${db.host}" />
			 <arg line="-p ${db.port}" />
			 <arg line="-noSecurityManager" />
		</java>
		<echo>Waiting for database</echo>
		<waitfor maxwait="2" maxwaitunit="minute" checkevery="1000" timeoutproperty="dbtimeout">
            	<socket server="${db.host}" port="${db.port}"/>
		</waitfor>
		<fail if="dbtimeout" message="Database not started" />
		<echo>Database started</echo>
	</target>
	
	<target name="stop.database">
		<java classname="org.apache.derby.drda.NetworkServerControl" fork="true" failonerror="true" resultproperty="javaresultcode">
		 <jvmarg value="-Dderby.stream.error.file=${db.log}" />
	         <classpath>
	           <pathelement location="${derby.lib}/derby.jar"/>
	           <pathelement location="${derby.lib}/derbynet.jar"/>
	         </classpath>
			 <arg line="shutdown" />
			 <arg line="-h ${db.host}" />
			 <arg line="-p ${db.port}" />			
		</java>
		<condition property="ok_result_code">
				<equals arg1="${javaresultcode}" arg2="0" />
		</condition>
		<fail unless="ok_result_code" message="Error during database operation." />
		<waitfor maxwait="3" maxwaitunit="second" checkevery="500" timeoutproperty="dbtimeout">
			<socket server="${db.host}" port="${db.port}"/>
		</waitfor>
		<fail unless="dbtimeout" message="Database not stopped" />
		<echo>Database stopped</echo>
	</target>
	
	<target name="load.dt.database" >
		<headless.application 
			workspace="${sampleworkspace}"
			targetproject="dtpopulate-rule" 
			dtname="dtsample" 
			driver="${db.driver}" 
			url="${db.url}" 
			user="${db.user}" 
			pwd="${db.pwd}" 
			info="'${db.query}'" 
		/>
	</target> 
	
	<target name="load.dt.file" >
		<headless.application 
			workspace="${sampleworkspace}"
			targetproject="dtpopulate-rule" 
			dtname="dtsample" 
			driver=" " 
			url="${file.url}" 
			user=" " 
			pwd=" " 
			info="${file.sep}" 
		/>
	</target>
	
	<target name="load.dt.xl" >
			<headless.application 
				workspace="${sampleworkspace}"
				targetproject="dtpopulate-rule" 
				dtname="dtsample" 
				driver=" " 
				url="${xl.url}" 
				user=" " 
				pwd=" " 
				info="${xl.sheet}" 
			/>
	</target>
</project>
